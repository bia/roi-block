package plugins.stef.roi.bloc;

import icy.main.Icy;
import icy.plugin.PluginLauncher;
import icy.plugin.PluginLoader;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;
import plugins.adufour.protocols.Protocols;

/**
 * Bundle of blocks (Protocols) to perform different operations on ROI (Region Of Interest).<br>
 * <br>
 * Supported operations:<br>
 * <ul>
 * <li>Create rectangular ROI (2D, 3D, 4D or 5D)</li>
 * <li>Filter by size</li>
 * <li>Perform boolean operation between two set of ROI</li>
 * <li>Get / set ROI position</li>
 * <li>Get / set ROI size</li>
 * <li>Get / set ROI properties (name, color)</li>
 * </ul>
 * <br>
 * 
 * @author stephane
 */
public class RoiBlocks extends Plugin implements PluginLibrary
{

    public static void main(String[] args)
    {
        Icy.main(args);
        PluginLauncher.start(PluginLoader.getPlugin(Protocols.class.getName()));
    }
}
