/**
 * 
 */
package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.system.IcyExceptionHandler;
import icy.util.StringUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarString;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to set the name of a ROI set
 * 
 * @author Stephane
 */
public class SetROIName extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected VarString name = new VarString("Name", "ROI");
    protected VarString numSuffix = new VarString("Number suffix", "_%d");

    @Override
    public void run()
    {
        int i = 0;
        for (ROI roi : roiSet)
        {
            if (roi != null)
            {
                String newName = name.getValue();
                final String suf = numSuffix.getValue();

                if (!StringUtil.isEmpty(suf))
                {
                    try
                    {
                        newName += String.format(suf, Integer.valueOf(i));
                    }
                    catch (Exception e)
                    {
                        System.err.println("Warning: block 'Set ROI Name' cannot parse 'Number suffix' field:");
                        IcyExceptionHandler.showErrorMessage(e, false);
                        System.err.println("Default suffix used.");
                        newName += String.format("_%d", Integer.valueOf(i));
                    }
                }

                roi.setName(newName);
                i++;
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("name", name);
        inputMap.add("suffix", numSuffix);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        // none here
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}