package plugins.stef.roi.bloc.property;

import java.awt.Color;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.system.IcyExceptionHandler;
import icy.util.StringUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarROIDescriptor;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Compute and return value of a given {@link ROIDescriptor} for the specified ROI
 * 
 * @author Stephane
 */
public class GetROIDescriptorValue extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    // we use ROI array for convenience here but only the first ROI is considered
    final protected VarROIArray rois = new VarROIArray("ROI", null);
    final protected VarSequence sequence = new VarSequence("Sequence", null);
    final protected VarROIDescriptor descriptor = new VarROIDescriptor("Value of");
    final protected VarMutable value = new VarMutable("Value", Double.class);

    @Override
    public void run()
    {
        final Sequence seq = sequence.getValue();
        final String descriptorId = descriptor.getValue();
        final ROIDescriptor roiDescriptor = ROIDescriptor.getDescriptor(descriptorId);

        if (roiDescriptor == null)
            throw new VarException(descriptor, "Cannot found '" + descriptorId + "' ROI descriptor !");

        for (ROI roi : rois.getValue())
        {
            if (roi != null)
            {
                try
                {
                    final Object res = roiDescriptor.compute(roi, seq);

                    if (res instanceof Number)
                    {
                        value.setType(Double.class);
                        value.setValue(Double.valueOf(((Number) res).doubleValue()));
                    }
                    else if (res instanceof Color)
                    {
                        final Color color = (Color) res;

                        String resString = StringUtil.toHexaString(color.getAlpha(), 2)
                                + StringUtil.toHexaString(color.getRed(), 2)
                                + StringUtil.toHexaString(color.getGreen(), 2)
                                + StringUtil.toHexaString(color.getBlue(), 2);

                        // set color value in form of String
                        value.setType(String.class);
                        value.setValue(resString.toUpperCase());
                    }
                    else if (res instanceof String)
                    {
                        value.setType(String.class);
                        value.setValue(res);
                    }
                    else if (res != null)
                    {
                        value.setType(String.class);
                        value.setValue(res.toString());
                    }
                    else
                    {
                        value.setType(String.class);
                        value.setValue("");
                    }
                }
                catch (Exception e)
                {
                    IcyExceptionHandler.showErrorMessage(e, false, true);
                    value.setType(String.class);
                    value.setValue("");
                }

                break;
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", rois);
        inputMap.add("sequence", sequence);
        inputMap.add("descriptors", descriptor);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("value", value);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
