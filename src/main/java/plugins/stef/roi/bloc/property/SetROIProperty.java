package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarString;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to set value (in String format) of a ROI property (name, opacity, custom...)
 * 
 * @author Stephane
 */
public class SetROIProperty extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected VarString propertyName = new VarString("Property", "name");
    protected VarString value = new VarString("Value", "");

    @Override
    public void run()
    {
        final String n = propertyName.getValue();
        final String v = value.getValue();

        for (ROI roi : roiSet)
            if (roi != null)
                roi.setProperty(n, v);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("property", propertyName);
        inputMap.add("value", value);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
