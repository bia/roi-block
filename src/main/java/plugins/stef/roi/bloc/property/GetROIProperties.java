/**
 * 
 */
package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;

import java.awt.Color;

import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarColor;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarFloat;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarString;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to retrieve properties of a ROI (name, color, opacity...)
 * 
 * @author Stephane
 */
public class GetROIProperties extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    // we use ROI array for convenience here but only the first ROI is considered
   protected VarROIArray roiSet = new VarROIArray("ROI", null);
    protected VarString name = new VarString("Name", "");
    protected VarColor color = new VarColor("Color", Color.green);
    protected VarFloat opacity = new VarFloat("Opacity", 0.2f);
    protected VarDouble stroke = new VarDouble("Stroke", 2d);
    protected VarBoolean showName = new VarBoolean("Show name", Boolean.FALSE);

    @Override
    public void run()
    {
        for (ROI roi : roiSet)
        {
            if (roi != null)
            {
                name.setValue(roi.getName());
                color.setValue(roi.getColor());
                opacity.setValue(Float.valueOf(roi.getOpacity()));
                stroke.setValue(Double.valueOf(roi.getStroke()));
                showName.setValue(Boolean.valueOf(roi.getShowName()));

                // stop here
                break;
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("roi", roiSet);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("name", name);
        outputMap.add("color", color);
        outputMap.add("opacity", opacity);
        outputMap.add("stroke", stroke);
        outputMap.add("showName", showName);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}