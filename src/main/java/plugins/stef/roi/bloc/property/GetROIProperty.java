package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarString;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to retrieve value (in String format) of a ROI property (name, opacity, custom...)
 * 
 * @author Stephane
 */
public class GetROIProperty extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    // we use ROI array for convenience here but only the first ROI is considered
    protected VarROIArray roiSet = new VarROIArray("ROI", null);
    protected VarString propertyName = new VarString("Property", "name");
    protected VarString value = new VarString("Value", "");

    @Override
    public void run()
    {
        for (ROI roi : roiSet)
        {
            if (roi != null)
            {
                value.setValue(roi.getProperty(propertyName.getValue()));

                // stop here
                break;
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("roi", roiSet);
        inputMap.add("property", propertyName);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("value", value);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
