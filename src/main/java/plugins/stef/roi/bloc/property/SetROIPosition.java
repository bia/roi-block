/**
 * 
 */
package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.type.point.Point5D;
import icy.type.point.Point5D.Double;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to set the position of a ROI set
 * 
 * @author Stephane
 */
public class SetROIPosition extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected VarDouble posX = new VarDouble("position X", 0d);
    protected VarDouble posY = new VarDouble("position Y", 0d);
    protected VarDouble posZ = new VarDouble("position Z", -1d);
    protected VarDouble posT = new VarDouble("position T", -1d);
    protected VarDouble posC = new VarDouble("position C", -1d);
    protected VarBoolean useCenter = new VarBoolean("Set XYZ Center", false);

    @Override
    public void run()
    {
        final Point5D.Double pos = new Point5D.Double(posX.getValue().doubleValue(), posY.getValue().doubleValue(),
                posZ.getValue().doubleValue(), posT.getValue().doubleValue(), posC.getValue().doubleValue());

        for (ROI roi : roiSet)
        {
            if ((roi != null) && roi.canSetPosition())
            {
                if (useCenter.getValue().booleanValue())
                {
                    Rectangle5D bounds = roi.getBounds5D();
                    Point5D.Double newPos = (Double) pos.clone();
                    newPos.setX(pos.getX() - bounds.getSizeX() / 2);
                    newPos.setY(pos.getY() - bounds.getSizeY() / 2);
                    newPos.setZ(pos.getZ() - bounds.getSizeZ() / 2);
                    roi.setPosition5D(newPos);
                }
                else
                {
                    roi.setPosition5D(pos);
                }
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("posX", posX);
        inputMap.add("posY", posY);
        inputMap.add("posZ", posZ);
        inputMap.add("posT", posT);
        inputMap.add("posC", posC);
        inputMap.add("useXYZCenter", useCenter);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
