package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.type.point.Point5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to set the C position of a ROI set
 * 
 * @author Stephane
 */
public class SetROIPositionC extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected VarDouble posC = new VarDouble("position C", -1d);

    @Override
    public void run()
    {
        for (ROI roi : roiSet)
        {
            if ((roi != null) && roi.canSetPosition())
            {
                final Point5D pos = roi.getPosition5D();
                pos.setC(posC.getValue().doubleValue());
                roi.setPosition5D(pos);
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("posC", posC);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}