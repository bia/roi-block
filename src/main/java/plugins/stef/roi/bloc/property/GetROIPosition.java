package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to get the position of a ROI
 * 
 * @author Stephane
 */
public class GetROIPosition extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    // we use ROI array for convenience here but only the first ROI is considered
    protected VarROIArray roiSet = new VarROIArray("ROI", null);
    protected VarDouble posX = new VarDouble("position X", 0d);
    protected VarDouble posY = new VarDouble("position Y", 0d);
    protected VarDouble posZ = new VarDouble("position Z", 0d);
    protected VarDouble posT = new VarDouble("position T", 0d);
    protected VarDouble posC = new VarDouble("position C", 0d);

    protected VarDouble posXCenter = new VarDouble("position X(center)", 0d);
    protected VarDouble posYCenter = new VarDouble("position Y(center)", 0d);
    protected VarDouble posZCenter = new VarDouble("position Z(center)", 0d);

    @Override
    public void run()
    {
        for (ROI roi : roiSet)
        {
            if (roi != null)
            {
                final Rectangle5D bounds = roi.getBounds5D();

                posX.setValue(Double.valueOf(bounds.getX()));
                posY.setValue(Double.valueOf(bounds.getY()));
                posZ.setValue(Double.valueOf(bounds.getZ()));
                posT.setValue(Double.valueOf(bounds.getT()));
                posC.setValue(Double.valueOf(bounds.getC()));

                posXCenter.setValue(Double.valueOf(bounds.getCenterX()));
                posYCenter.setValue(Double.valueOf(bounds.getCenterY()));
                posZCenter.setValue(Double.valueOf(bounds.getCenterZ()));

                // stop here
                break;
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("roi", roiSet);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("posX", posX);
        outputMap.add("posY", posY);
        outputMap.add("posZ", posZ);
        outputMap.add("posT", posT);
        outputMap.add("posC", posC);

        outputMap.add("posXCenter", posXCenter);
        outputMap.add("posYCenter", posYCenter);
        outputMap.add("posZCenter", posZCenter);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
