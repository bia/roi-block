/**
 * 
 */
package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;

import java.awt.Color;

import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarColor;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to set the color of a ROI set
 * 
 * @author Stephane
 */
public class SetROIColor extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected final VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected final VarColor color = new VarColor("Color", Color.green);

    @Override
    public void run()
    {
        for (ROI roi : roiSet)
            if (roi != null)
                roi.setColor(color.getValue());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("color", color);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        // none here
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}