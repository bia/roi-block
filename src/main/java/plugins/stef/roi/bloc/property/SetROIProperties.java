/**
 * 
 */
package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.system.IcyExceptionHandler;
import icy.util.StringUtil;

import java.awt.Color;

import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarColor;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarFloat;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarString;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to set properties of a ROI (name, color, opacity...)
 * 
 * @author Stephane
 */
public class SetROIProperties extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected VarString name = new VarString("Name", "");
    protected VarString numSuffix = new VarString("Number suffix", "_%d");
    protected VarColor color = new VarColor("Color", Color.green);
    protected VarFloat opacity = new VarFloat("Opacity", 0.2f);
    protected VarDouble stroke = new VarDouble("Stroke", 2d);
    protected VarBoolean showName = new VarBoolean("Show name", Boolean.FALSE);

    @Override
    public void run()
    {
        int i = 0;
        for (ROI roi : roiSet)
        {
            if (roi != null)
            {
                String newName = name.getValue();
                final String suf = numSuffix.getValue();

                if (!StringUtil.isEmpty(suf))
                {
                    try
                    {
                        newName += String.format(suf, Integer.valueOf(i));
                    }
                    catch (Exception e)
                    {
                        System.err.println("Warning: block 'Set ROI Properties' cannot parse 'Number suffix' field:");
                        IcyExceptionHandler.showErrorMessage(e, false);
                        System.err.println("Default suffix used.");
                        newName += String.format("_%d", Integer.valueOf(i));
                    }
                }

                roi.setName(newName);
                roi.setColor(color.getValue());
                roi.setOpacity(opacity.getValue().floatValue());
                roi.setStroke(stroke.getValue().doubleValue());
                roi.setShowName(showName.getValue().booleanValue());
                i++;
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("name", name);
        inputMap.add("suffix", numSuffix);
        inputMap.add("color", color);
        inputMap.add("opacity", opacity);
        inputMap.add("stroke", stroke);
        inputMap.add("showName", showName);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}