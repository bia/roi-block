/**
 * 
 */
package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.type.dimension.Dimension5D;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to set the size of a ROI set
 * 
 * @author Stephane
 */
public class SetROISize extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected VarDouble sizeX = new VarDouble("size X", 0d);
    protected VarDouble sizeY = new VarDouble("size Y", 0d);
    protected VarDouble sizeZ = new VarDouble("size Z", -1d);
    protected VarDouble sizeT = new VarDouble("size T", -1d);
    protected VarDouble sizeC = new VarDouble("size C", -1d);

    @Override
    public void run()
    {
        final Dimension5D size = new Dimension5D.Double(sizeX.getValue().doubleValue(), sizeY.getValue().doubleValue(),
                sizeZ.getValue().doubleValue(), sizeT.getValue().doubleValue(), sizeC.getValue().doubleValue());

        for (ROI roi : roiSet)
        {
            if ((roi != null) && roi.canSetBounds())
            {
                final Rectangle5D bnd = roi.getBounds5D();

                bnd.setSizeX(size.getSizeX());
                bnd.setSizeY(size.getSizeY());
                bnd.setSizeZ(size.getSizeZ());
                bnd.setSizeT(size.getSizeT());
                bnd.setSizeC(size.getSizeC());

                roi.setBounds5D(bnd);
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("sizeX", sizeX);
        inputMap.add("sizeY", sizeY);
        inputMap.add("sizeZ", sizeZ);
        inputMap.add("sizeT", sizeT);
        inputMap.add("sizeC", sizeC);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}