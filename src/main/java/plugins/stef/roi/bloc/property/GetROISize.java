/**
 * 
 */
package plugins.stef.roi.bloc.property;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to get the size of a ROI
 * 
 * @author Stephane
 */
public class GetROISize extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    // we use ROI array for convenience here but only the first ROI is considered
    protected VarROIArray roiSet = new VarROIArray("ROI", null);
    protected VarDouble sizeX = new VarDouble("size X", 0d);
    protected VarDouble sizeY = new VarDouble("size Y", 0d);
    protected VarDouble sizeZ = new VarDouble("size Z", 0d);
    protected VarDouble sizeT = new VarDouble("size T", 0d);
    protected VarDouble sizeC = new VarDouble("size C", 0d);

    @Override
    public void run()
    {
        for (ROI roi : roiSet)
        {
            if (roi != null)
            {
                final Rectangle5D bnd = roi.getBounds5D();

                sizeX.setValue(Double.valueOf(bnd.getSizeX()));
                sizeY.setValue(Double.valueOf(bnd.getSizeY()));
                sizeZ.setValue(Double.valueOf(bnd.getSizeZ()));
                sizeT.setValue(Double.valueOf(bnd.getSizeT()));
                sizeC.setValue(Double.valueOf(bnd.getSizeC()));

                // stop here
                break;
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("roi", roiSet);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("sizeX", sizeX);
        outputMap.add("sizeY", sizeY);
        outputMap.add("sizeZ", sizeZ);
        outputMap.add("sizeT", sizeT);
        outputMap.add("sizeC", sizeC);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}