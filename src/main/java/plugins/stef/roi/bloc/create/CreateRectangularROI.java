/**
 * 
 */
package plugins.stef.roi.bloc.create;

import java.awt.Rectangle;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.type.rectangle.Rectangle3D;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.kernel.roi.roi2d.ROI2DRectangle;
import plugins.kernel.roi.roi3d.ROI3DBox;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to create a Rectangular ROI (Region Of Interest).
 * 
 * @author Stephane
 */
public class CreateRectangularROI extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarSequence inputSequence = new VarSequence("Size from Sequence", null);
    protected VarInteger sizeX = new VarInteger("Size X", 100);
    protected VarInteger sizeY = new VarInteger("Size Y", 100);
    protected VarInteger sizeZ = new VarInteger("Size Z", -1);
    protected VarROIArray output = new VarROIArray("roi");

    public CreateRectangularROI()
    {
        super();

        // inputSequence.setToolTipText("If set the sequence dimensions are used to create the rectangular ROI");
        // sizeX.setToolTipText("Width of the output rectangular ROI");
        // sizeY.setToolTipText("Height of the output rectangular ROI");
        // sizeZ.setToolTipText("Z dimension size. If this value is > 1 a 3D rectangular ROI will be created, -1 is a special value to define infinite Z
        // dimension");
        // sizeT.setToolTipText("T dimension size. If this value is > 1 a 4D rectangular ROI will be created, -1 is a special value to define infinite T
        // dimension");
        // sizeC.setToolTipText("C dimension size. If this value is > 1 a 5D rectangular ROI will be created, -1 is a special value to define infinite C
        // dimension");
    }

    @Override
    public void run()
    {
        final int sx, sy, sz;

        // no input sequence
        if (inputSequence.getValue() == null)
        {
            sx = sizeX.getValue().intValue();
            sy = sizeY.getValue().intValue();
            sz = sizeZ.getValue().intValue();

            if (sx <= 0)
                throw new VarException(sizeX, "SizeX field should be > 0");
            if (sy <= 0)
                throw new VarException(sizeY, "SizeY field should be > 0");
            if ((sz < -1) || (sz == 0))
                throw new VarException(sizeZ, "SizeZ field should be > 0 or equals to -1 (infinite Z dimension)");
        }
        else
        {
            final Rectangle5D.Integer bnd = inputSequence.getValue().getBounds5D();

            sx = bnd.sizeX;
            sy = bnd.sizeY;
            if (bnd.isInfiniteZ() || (bnd.sizeZ == 1))
                sz = -1;
            else
                sz = bnd.sizeZ;
        }

        ROI result;

        // 3D ROI
        if (sz > 1)
            result = new ROI3DBox(new Rectangle3D.Integer(0, 0, 0, sx, sy, sz));
        // 2D ROI
        else
            result = new ROI2DRectangle(new Rectangle(0, 0, sx, sy));

        output.setValue(new ROI[] {result});
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", inputSequence);
        inputMap.add("sizeX", sizeX);
        inputMap.add("sizeY", sizeY);
        inputMap.add("sizeZ", sizeZ);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}