package plugins.stef.roi.bloc.io;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.type.collection.CollectionUtil;
import icy.util.XMLUtil;

import java.io.File;
import java.util.List;

import org.w3c.dom.Document;

import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to save a set of ROI to a file
 * 
 * @author Stephane
 */
public class SaveRoisToFile extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    VarROIArray rois = new VarROIArray("ROI(s)");
    VarMutable file = new VarMutable("File", null)
    {
        @Override
        public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
        {
            return (String.class == source.getType()) || (File.class == source.getType());
        }
    };

    @Override
    public void run()
    {
        final ROI[] array = rois.getValue();
        if (array == null)
            return;

        final List<ROI> roiList = CollectionUtil.asList(array);
        if (roiList.isEmpty())
            return;

        final Object obj = file.getValue();
        if (obj == null)
            return;

        final File f;

        if (obj instanceof String)
            f = new File((String) obj);
        else
            f = (File) obj;

        // create new XML doc
        final Document doc = XMLUtil.createDocument(true);
        // save ROIs into the XML node
        ROI.saveROIsToXML(XMLUtil.getRootElement(doc), roiList);
        // then save XML doc to file
        XMLUtil.saveDocument(doc, f);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", rois);
        inputMap.add("file", file);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
