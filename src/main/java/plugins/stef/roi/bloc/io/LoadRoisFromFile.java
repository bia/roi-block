package plugins.stef.roi.bloc.io;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.util.XMLUtil;

import java.io.File;
import java.util.List;

import org.w3c.dom.Document;

import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to load a set of ROI from a file
 * 
 * @author Stephane
 */
public class LoadRoisFromFile extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    VarMutable file = new VarMutable("File", null)
    {
        @Override
        public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
        {
            return (String.class == source.getType()) || (File.class == source.getType());
        }
    };
    VarROIArray out = new VarROIArray("ROI(s)");

    @Override
    public void run()
    {
        final Object obj = file.getValue();

        if (obj == null)
            return;

        final File f;

        if (obj instanceof String)
            f = new File((String) obj);
        else
            f = (File) obj;

        // if (f.isDirectory())
        // throw new VarException("file should not be a directory.");

        // open XML doc
        final Document doc = XMLUtil.loadDocument(f);
        // load ROIS from XML root node
        final List<ROI> rois = ROI.loadROIsFromXML(XMLUtil.getRootElement(doc));

        out.setValue(rois.toArray(new ROI[rois.size()]));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("file", file);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", out);

    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
