/**
 * 
 */
package plugins.stef.roi.bloc.op;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to adjust a set of ROIs we want to move from a source {@link Sequence} to a destination {@link Sequence}.<br>
 * Resulting ROIs are translated and scaled according to the origin information of both Sequence.
 * 
 * @author Stephane
 */
public class MoveROIToSequence extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected final VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected final VarSequence varSrcSeq = new VarSequence("Source", null);
    protected final VarSequence varDstSeq = new VarSequence("Destination", null);
    protected final VarBoolean translate = new VarBoolean("Translate", Boolean.TRUE);
    protected final VarBoolean scale = new VarBoolean("Scale", Boolean.TRUE);
    protected final VarROIArray roiResultSet = new VarROIArray("ROI(s)", null);

    @Override
    public void run()
    {
        final ROI[] rois = roiSet.getValue();

        // nothing to do
        if (rois == null)
        {
            roiResultSet.setValue(new ROI[0]);
            return;
        }

        final ROI[] result = new ROI[rois.length];
        final Sequence srcSeq = varSrcSeq.getValue();
        final Sequence dstSeq = varDstSeq.getValue();

        final boolean t = translate.getValue().booleanValue();
        final boolean s = scale.getValue().booleanValue();

        try
        {
            for (int r = 0; r < rois.length; r++)
                result[r] = ROIUtil.adjustToSequence(rois[r], srcSeq, dstSeq, t, s);

            roiResultSet.setValue(result);
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }

    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("seqSrc", varSrcSeq);
        inputMap.add("seqDst", varDstSeq);
        inputMap.add("translate", translate);
        inputMap.add("scale", scale);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("roisRes", roiResultSet);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}