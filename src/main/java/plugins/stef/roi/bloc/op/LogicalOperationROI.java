package plugins.stef.roi.bloc.op;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.BooleanMask2D;
import icy.roi.BooleanMask3D;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.type.collection.CollectionUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block computing logical operation between 2 groups of ROIs to retain only a sub part of the first group depending the selected Logical Operation.
 * Examples<br>
 * - Logic operator A_CONTAINING_B means it will keep all ROIs from group A which contains at least one ROI from group B.<br>
 * - Logic operator A_NOT_CONTAINED_IN_B means it will keep all ROIs from group A which are not contained by any ROI from group B.
 * 
 * @author Stephane
 */
public class LogicalOperationROI extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    public static enum LogicOperator
    {
        A_CONTAINED_IN_B, A_CONTAINING_B, A_INTERSECTING_B, A_NOT_CONTAINED_IN_B, A_NOT_CONTAINING_B,
        A_NOT_INTERSECTING_B
    }

    protected VarROIArray roiSetA = new VarROIArray("ROI(s) group A", null);
    protected VarROIArray roiSetB = new VarROIArray("ROI(s) group B", null);
    protected VarEnum<LogicOperator> op = new VarEnum<LogicOperator>("Keep", LogicOperator.A_CONTAINED_IN_B);
    protected VarBoolean copyRois = new VarBoolean("Copy ROIs", true);
    protected VarROIArray output = new VarROIArray("Result");

    @Override
    public void run()
    {
        try
        {
            final List<ROI> result = doLogicalOperation(CollectionUtil.asList(roiSetA.getValue()),
                    CollectionUtil.asList(roiSetB.getValue()), op.getValue(), copyRois.getValue());

            output.setValue(result.toArray(new ROI[result.size()]));
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("roiA", roiSetA);
        inputMap.add("roiB", roiSetB);
        inputMap.add("op", op);
        inputMap.add("generate copies", copyRois);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }

    /**
     * Apply a logical operation between 2 set of ROIS and obtain the result.
     * 
     * @param roiSetA
     *        first set of ROI
     * @param roiSetB
     *        second set of ROI
     * @param logicOp
     *        logical operation to apply between the 2 sets of ROI
     * @param copyRois
     *        perform a copy of originals ROIs (so they are never modified)
     * @return result ROIs from the given logical operation between the 2 sets of ROI
     * @throws InterruptedException
     */
    public static List<ROI> doLogicalOperation(Collection<ROI> roiSetA, Collection<ROI> roiSetB, LogicOperator logicOp,
            boolean copyRois) throws InterruptedException
    {
        final List<ROI> result = new ArrayList<ROI>();
        final Map<ROI, SoftReference<BooleanMask2D>> masks2d = new HashMap<>();
        final Map<ROI, SoftReference<BooleanMask3D>> masks3d = new HashMap<>();

        try
        {
            for (ROI roi : roiSetA)
            {
                if (roi instanceof ROI2D)
                    masks2d.put(roi, new SoftReference<>(((ROI2D) roi).getBooleanMask(true)));
                else if (roi instanceof ROI3D)
                    masks3d.put(roi, new SoftReference<>(((ROI3D) roi).getBooleanMask(true)));
            }

            for (ROI roi : roiSetB)
            {
                if (roi instanceof ROI2D)
                    masks2d.put(roi, new SoftReference<>(((ROI2D) roi).getBooleanMask(true)));
                else if (roi instanceof ROI3D)
                    masks3d.put(roi, new SoftReference<>(((ROI3D) roi).getBooleanMask(true)));
            }
        }
        catch (OutOfMemoryError error)
        {
            // not enough memory to build out the masks cache, we will use what we have...

        }

        for (ROI roiA : roiSetA)
        {
            SoftReference<?> ref;

            if (roiA != null)
            {
                BooleanMask2D mask2dA = null;
                BooleanMask3D mask3dA = null;

                // try to get masks from hashmap
                ref = masks2d.get(roiA);
                if (ref != null)
                    mask2dA = (BooleanMask2D) ref.get();
                else
                {
                    ref = masks3d.get(roiA);
                    if (ref != null)
                        mask3dA = (BooleanMask3D) ref.get();
                }

                boolean cond = (logicOp == LogicOperator.A_NOT_CONTAINED_IN_B)
                        || (logicOp == LogicOperator.A_NOT_CONTAINING_B)
                        || (logicOp == LogicOperator.A_NOT_INTERSECTING_B);
                boolean done = false;

                for (ROI roiB : roiSetB)
                {
                    BooleanMask2D mask2dB = null;
                    BooleanMask3D mask3dB = null;

                    // try to get mask from hashmap
                    ref = masks2d.get(roiB);
                    if (ref != null)
                        mask2dB = (BooleanMask2D) ref.get();
                    else
                    {
                        ref = masks3d.get(roiB);
                        if (ref != null)
                            mask3dB = (BooleanMask3D) ref.get();
                    }

                    switch (logicOp)
                    {
                        default:
                        case A_CONTAINED_IN_B:
                        case A_NOT_CONTAINED_IN_B:
                            // we have the 2D masks ? --> use them
                            if ((mask2dA != null) && (mask2dB != null))
                            {
                                if (mask2dB.contains(mask2dA))
                                {
                                    done = true;
                                    cond = (logicOp == LogicOperator.A_CONTAINED_IN_B);
                                }
                            }
                            // we have the 3D masks ? --> use them
                            else if ((mask3dA != null) && (mask3dB != null))
                            {
                                if (mask3dB.contains(mask3dA))
                                {
                                    done = true;
                                    cond = (logicOp == LogicOperator.A_CONTAINED_IN_B);
                                }
                            }
                            // do the generic test
                            else if (roiB.contains(roiA))
                            {
                                done = true;
                                cond = (logicOp == LogicOperator.A_CONTAINED_IN_B);
                            }
                            break;

                        case A_CONTAINING_B:
                        case A_NOT_CONTAINING_B:
                            // we have the 2D masks ? --> use them
                            if ((mask2dA != null) && (mask2dB != null))
                            {
                                if (mask2dA.contains(mask2dB))
                                {
                                    done = true;
                                    cond = (logicOp == LogicOperator.A_CONTAINING_B);
                                }
                            }
                            // we have the 3D masks ? --> use them
                            else if ((mask3dA != null) && (mask3dB != null))
                            {
                                if (mask3dA.contains(mask3dB))
                                {
                                    done = true;
                                    cond = (logicOp == LogicOperator.A_CONTAINING_B);
                                }
                            }
                            // do the generic test
                            else if (roiA.contains(roiB))
                            {
                                done = true;
                                cond = (logicOp == LogicOperator.A_CONTAINING_B);
                            }
                            break;

                        case A_INTERSECTING_B:
                        case A_NOT_INTERSECTING_B:
                            // we have the 2D masks ? --> use them
                            if ((mask2dA != null) && (mask2dB != null))
                            {
                                if (mask2dA.intersects(mask2dB))
                                {
                                    done = true;
                                    cond = (logicOp == LogicOperator.A_INTERSECTING_B);
                                }
                            }
                            // we have the 3D masks ? --> use them
                            else if ((mask3dA != null) && (mask3dB != null))
                            {
                                if (mask3dA.intersects(mask3dB))
                                {
                                    done = true;
                                    cond = (logicOp == LogicOperator.A_INTERSECTING_B);
                                }
                            }
                            // do the generic test
                            else if (roiA.intersects(roiB))
                            {
                                done = true;
                                cond = (logicOp == LogicOperator.A_INTERSECTING_B);
                            }
                            break;
                    }

                    // can stop here
                    if (done)
                        break;
                }

                // condition verified ?
                if (cond)
                    result.add((copyRois ? roiA.getCopy() : roiA));
            }
        }

        return result;
    }
}
