/**
 * 
 */
package plugins.stef.roi.bloc.op;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.type.collection.CollectionUtil;
import icy.util.ShapeUtil.BooleanOperator;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to merge two set of ROIs together applying a logic Boolean Operation (OR, AND or XOR)
 * 
 * @author Stephane
 */
public class BooleanOperationROI extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray roiSet1 = new VarROIArray("ROI(s) first set", null);
    protected VarROIArray roiSet2 = new VarROIArray("ROI(s) second set", null);
    protected VarEnum<BooleanOperator> op = new VarEnum<BooleanOperator>("Operation", BooleanOperator.OR);
    protected VarROIArray output = new VarROIArray("Result");

    @Override
    public void run()
    {
        try
        {
            final List<ROI> result = doBooleanOperation(CollectionUtil.asList(roiSet1.getValue()),
                    CollectionUtil.asList(roiSet2.getValue()), op.getValue());

            output.setValue(result.toArray(new ROI[result.size()]));
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("roi1", roiSet1);
        inputMap.add("roi2", roiSet2);
        inputMap.add("op", op);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }

    /**
     * Merge two set of ROIs together applying a logic Boolean Operation (OR, AND or XOR)
     * 
     * @param roiSetA
     *        first set of ROI
     * @param roiSetB
     *        second set of ROI
     * @param op
     *        boolean operation to apply
     * @return result ROIs from the given boolean operation between the 2 sets of ROI
     * @throws InterruptedException
     * @throws UnsupportedOperationException
     */
    public static List<ROI> doBooleanOperation(Collection<ROI> roiSetA, Collection<ROI> roiSetB, BooleanOperator op)
            throws UnsupportedOperationException, InterruptedException
    {
        final List<ROI> result = new ArrayList<ROI>();

        for (ROI roi1 : roiSetA)
        {
            if (roi1 != null)
            {
                for (ROI roi2 : roiSetB)
                {
                    final ROI r = roi1.merge(roi2, op);

                    // we don't want empty ROI (intersection can produce a lot of them)
                    if ((r != null) && !r.getBounds5D().isEmpty())
                        result.add(r);
                }
            }
            else
            {
                // roi1 is null here
                for (ROI roi2 : roiSetB)
                {
                    switch (op)
                    {
                        case OR:
                        case XOR:
                            if (roi2 != null)
                                result.add(roi2.getCopy());
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        return result;
    }
}
