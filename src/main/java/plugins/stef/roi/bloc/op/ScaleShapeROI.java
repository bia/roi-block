/**
 * 
 */
package plugins.stef.roi.bloc.op;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to scale a set of Shape ROIs (2D or 3D) along X, Y and Z axis.
 * 
 * @author Stephane
 */
public class ScaleShapeROI extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected final VarROIArray roiSet = new VarROIArray("Shape ROI(s)", null);
    protected final VarDouble scaleX = new VarDouble("X", 1d);
    protected final VarDouble scaleY = new VarDouble("Y", 1d);
    protected final VarDouble scaleZ = new VarDouble("Z", 1d);

    @Override
    public void run()
    {
        final double sx = scaleX.getValue().doubleValue();
        final double sy = scaleY.getValue().doubleValue();
        final double sz = scaleZ.getValue().doubleValue();

        for (ROI roi : roiSet)
            if (roi != null)
                ROIUtil.scale(roi, sx, sy, sz);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("scaleX", scaleX);
        inputMap.add("scaleY", scaleY);
        inputMap.add("scaleZ", scaleZ);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}