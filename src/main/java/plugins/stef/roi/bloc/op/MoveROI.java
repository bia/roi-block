/**
 * 
 */
package plugins.stef.roi.bloc.op;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.type.point.Point5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to move a set of ROIs (absolute or relative) along X, Y and Z coordinates.
 * 
 * @author Stephane
 */
public class MoveROI extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected VarDouble movX = new VarDouble("X", 0d);
    protected VarDouble movY = new VarDouble("Y", 0d);
    protected VarDouble movZ = new VarDouble("Z", 0d);
    protected VarBoolean setZ = new VarBoolean("set Z", Boolean.FALSE);
    protected VarBoolean relative = new VarBoolean("Relative", Boolean.FALSE);

    @Override
    public void run()
    {
        final Point5D mov = new Point5D.Double(movX.getValue().doubleValue(), movY.getValue().doubleValue(),
                movZ.getValue().doubleValue(), 0d, 0d);
        final boolean sz = setZ.getValue().booleanValue();
        final boolean r = relative.getValue().booleanValue();

        for (ROI roi : roiSet)
        {
            if ((roi != null) && roi.canSetPosition())
            {
                final Point5D pos = roi.getPosition5D();

                if (r)
                    pos.setLocation(pos.getX() + mov.getX(), pos.getY() + mov.getY(),
                            pos.getZ() + (sz ? mov.getZ() : 0d), pos.getT(), pos.getC());
                else
                    pos.setLocation(mov.getX(), mov.getY(), sz ? mov.getZ() : pos.getZ(), pos.getT(), pos.getC());

                roi.setPosition5D(pos);
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", roiSet);
        inputMap.add("movX", movX);
        inputMap.add("movY", movY);
        inputMap.add("movZ", movZ);
        inputMap.add("setZ", setZ);
        inputMap.add("relative", relative);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}