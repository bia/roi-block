/**
 * 
 */
package plugins.stef.roi.bloc.op;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import icy.system.IcyHandledException;
import icy.type.collection.CollectionUtil;
import icy.type.dimension.Dimension3D;
import icy.type.dimension.Dimension5D;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * This block allows users to use the watershed separation function available on the ROI tools on the ribbon of Icy.
 * ROIs are supposed to be 2D ROIs.
 * Seed ROIs are optional. If no seeds are provided, they will be computed using a distance map on the image.
 * The result ROIs will be the separated components.
 * The result seed ROIs will be either the input seeds or the ones computed by the algorithm.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class SeparateROIWatershed extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray varInTargetRois;
    protected VarROIArray varInSeedRois;

    @Override
    public void declareInput(VarList inputMap)
    {
        varInTargetRois = new VarROIArray("ROI(s)");
        varInSeedRois = new VarROIArray("Seed ROI(s)");
        inputMap.add("inputRois", varInTargetRois);
        inputMap.add("inputSeeds", varInSeedRois);
    }

    protected VarROIArray varOutRois;
    protected VarROIArray varOutSeeds;

    @Override
    public void declareOutput(VarList outputMap)
    {
        varOutRois = new VarROIArray("Separated ROI(s)");
        varOutSeeds = new VarROIArray("Seed ROI(s)");
        outputMap.add("outputRois", varOutRois);
        outputMap.add("outputSeeds", varOutSeeds);
    }

    @Override
    public void run()
    {
        final ROI[] rois = varInTargetRois.getValue();
        final ROI[] seeds = varInSeedRois.getValue();
        List<ROI> result = new ArrayList<ROI>();
        List<ROI> seedList;
        if (rois != null)
        {
            try
            {
                if (seeds != null && seeds.length > 0)
                {
                    seedList = CollectionUtil.asArrayList(seeds);
                    result = ROIUtil.computeWatershedSeparation(Arrays.asList(rois), seedList, getDimensionSize(rois),
                            new Dimension3D.Double(1d, 1d, 1d));
                }
                else
                {
                    seedList = new ArrayList<ROI>();
                    result = ROIUtil.computeWatershedSeparation(Arrays.asList(rois), getDimensionSize(rois),
                            new Dimension3D.Double(1d, 1d, 1d), seedList);
                }

            }
            catch (Exception e)
            {
                throw new IcyHandledException("Error on watershed separation: " + e.getMessage(), e);
            }

            varOutRois.setValue(result.toArray(new ROI[result.size()]));
            varOutSeeds.setValue(seedList.toArray(new ROI[seedList.size()]));
        }
    }

    private Dimension5D getDimensionSize(ROI[] rois)
    {
        Dimension5D.Double dims = new Dimension5D.Double();
        for (ROI roi : rois)
        {
            Rectangle5D roiBounds = roi.getBounds5D();
            dims.sizeX = Math.max(dims.sizeX, roiBounds.getMaxX() + 10);
            dims.sizeY = Math.max(dims.sizeY, roiBounds.getMaxY() + 10);
            dims.sizeZ = Math.max(dims.sizeZ, roiBounds.getMaxZ());
            dims.sizeT = Math.max(dims.sizeT, roiBounds.getMaxT());
            dims.sizeC = Math.max(dims.sizeC, roiBounds.getMaxC());
        }

        if (!Double.isFinite(dims.sizeX) || dims.sizeX == 0)
            dims.sizeX = 1;
        if (!Double.isFinite(dims.sizeY) || dims.sizeY == 0)
            dims.sizeY = 1;
        if (!Double.isFinite(dims.sizeZ) || dims.sizeZ == 0)
            dims.sizeZ = 1;
        if (!Double.isFinite(dims.sizeT) || dims.sizeT == 0)
            dims.sizeT = 1;
        if (!Double.isFinite(dims.sizeC) || dims.sizeC == 0)
            dims.sizeC = 1;

        return dims;
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }

}
