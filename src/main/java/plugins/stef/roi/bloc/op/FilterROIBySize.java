/**
 * 
 */
package plugins.stef.roi.bloc.op;

import java.util.ArrayList;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to filter a set of ROIs by size
 * 
 * @author Stephane
 */
public class FilterROIBySize extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected VarDouble minimumSize = new VarDouble("Min size (px)", 10d);
    protected VarDouble maximumSize = new VarDouble("Max size (px)", 10000d);
    protected VarBoolean incBorder = new VarBoolean("Include border pixels", Boolean.FALSE);
    protected VarROIArray output = new VarROIArray("ROI(s)");

    @Override
    public void run()
    {
        final List<ROI> result = new ArrayList<ROI>();
        final double min = minimumSize.getValue().doubleValue();
        final double max = maximumSize.getValue().doubleValue();
        final boolean border = incBorder.getValue().booleanValue();

        try
        {
            for (ROI roi : roiSet)
            {
                if (roi != null)
                {
                    double num = roi.getNumberOfPoints();
                    if (border)
                        num += roi.getNumberOfContourPoints();

                    if ((num >= min) && (num <= max))
                        result.add(roi);
                }
            }

            output.setValue(result.toArray(new ROI[result.size()]));
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("roi", roiSet);
        inputMap.add("minSize", minimumSize);
        inputMap.add("maxSize", maximumSize);
        inputMap.add("incBorder", incBorder);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}