package plugins.stef.roi.bloc.op;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import icy.gui.dialog.MessageDialog;
import icy.image.colormap.IcyColorMap;
import icy.image.colormap.LinearColorMap;
import icy.plugin.interface_.PluginBundled;
import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.system.IcyExceptionHandler;
import icy.type.collection.CollectionUtil;
import icy.util.ReflectionUtil;
import icy.util.StringUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarText;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.kernel.roi.descriptor.measure.ROIInteriorDescriptor;
import plugins.stef.roi.bloc.RoiBlocks;
import plugins.tprovoost.sequenceblocks.infos.EzVarColormap;

/**
 * Plugin/block to color a set of ROIs from a specific descriptor and a given colormap.
 * 
 * @author Stephane
 */
public class ColorROI extends EzPlug implements ROIBlock, PluginBundled
{
    protected final VarROIArray roiSet;
    protected final VarSequence sequence;
    protected final EzVarText descriptors;
    protected final EzVarColormap colormap;
    protected final EzVarBoolean reverse;

    public ColorROI()
    {
        super();

        final List<ROIDescriptor> roiDescriptors = new ArrayList<ROIDescriptor>(ROIDescriptor.getDescriptors().keySet());
        // build list of descriptor id
        final List<String> descriptorsId = new ArrayList<String>();
        String sizeDescriptorId = null;

        for (ROIDescriptor descriptor : roiDescriptors)
        {
            final String id = descriptor.getId();

            // keep trace of size descriptor
            if (StringUtil.equals(ROIInteriorDescriptor.ID, id))
                sizeDescriptorId = id;

            descriptorsId.add(id);
        }

        // alpha sort
        Collections.sort(descriptorsId);

        roiSet = new VarROIArray("ROI(s)", null);
        sequence = new VarSequence("Sequence", null);
        descriptors = new EzVarText("Filter on", descriptorsId.toArray(new String[descriptorsId.size()]), descriptorsId.indexOf(sizeDescriptorId),
                Boolean.FALSE);
        colormap = new EzVarColormap("Color map", LinearColorMap.gray_);
        reverse = new EzVarBoolean("Reverse", false);
    }

    @Override
    protected void initialize()
    {
        addEzComponent(descriptors);
        addEzComponent(colormap);
        addEzComponent(reverse);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("roi", roiSet);
        inputMap.add("sequence", sequence);
        inputMap.add("descriptors", descriptors.getVariable());
        inputMap.add("colormap", colormap.getVariable());
        inputMap.add("reverse", reverse.getVariable());
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public void clean()
    {
        //
    }

    @Override
    protected void execute()
    {
        List<ROI> rois = null;
        Sequence seq;

        // headless / protocol mode --> get Sequence and ROI from variables
        if (isHeadLess())
        {
            seq = sequence.getValue();
            rois = CollectionUtil.asList(roiSet.getValue());
        }
        // interactive mode
        else
        {
            seq = getActiveSequence();

            if (seq != null)
            {
                // then try to get it from Sequence
                rois = seq.getROIs();

                if (rois.isEmpty())
                {
                    MessageDialog.showDialog("The selected Sequence doesn't contain any ROI", MessageDialog.INFORMATION_MESSAGE);
                }
            }
        }

        try
        {
            if (rois != null)
                colorROIs(rois, seq, descriptors.getValue(), colormap.getValue(), reverse.getValue().booleanValue());
        }
        catch (IllegalArgumentException e)
        {
            if (isHeadLess())
                throw new VarException(descriptors.getVariable(), e.getMessage());

            // just re-throw it
            throw e;
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }

    /**
     * Color a set of ROIs from a specific descriptor and a given colormap.
     * 
     * @param rois
     *        input ROIS to color
     * @param sequence
     *        input sequence used for ROI descriptor computation (can be null if not required)
     * @param descriptorId
     *        the {@link ROIDescriptor} id to use for coloring
     * @param colormap
     *        the colormap used to do ROI coloring
     * @param reverse
     *        reverse coloring
     * @throws IllegalArgumentException
     *         if given ROI descriptor id is not found
     */
    public static void colorROIs(Collection<ROI> rois, Sequence sequence, String descriptorId, IcyColorMap colormap, boolean reverse)
            throws IllegalArgumentException
    {
        final ROIDescriptor roiDescriptor = ROIDescriptor.getDescriptor(ROIDescriptor.getDescriptors().keySet(), descriptorId);

        if (roiDescriptor == null)
            throw new IllegalArgumentException("Cannot found '" + descriptorId + "' ROI descriptor !");

        final Map<ROI, Number> results = new HashMap<ROI, Number>();

        // find min and max
        double minValue = Double.MAX_VALUE;
        double maxValue = -Double.MAX_VALUE;

        final Object[] bounds = roiDescriptor.getBounds();
        if ((bounds != null) && (bounds[0] instanceof Number))
        {
            minValue = ((Number) bounds[0]).doubleValue();  
            maxValue = ((Number) bounds[1]).doubleValue();  
        }

        for (ROI roi : rois)
        {
            if (roi != null)
            {
                try
                {
                    final Object res = roiDescriptor.compute(roi, sequence);

                    // we only support Number result for coloring
                    if (res instanceof Number)
                    {
                        final double value = ((Number) res).doubleValue();

                        if (!Double.isNaN(value) && !Double.isInfinite(value))
                        {
                            // adjust min/max
                            if (value < minValue)
                                minValue = value;
                            if (value > maxValue)
                                maxValue = value;

                            // store result in a map
                            results.put(roi, (Number) res);
                        }
                    }
                }
                catch (Exception e)
                {
                    IcyExceptionHandler.showErrorMessage(e, false, true);
                }
            }
        }

        final double range = maxValue - minValue;

        for (ROI roi : rois)
        {
            if (roi != null)
            {
                final Number res = results.get(roi);
                final Color color;

                if (res != null)
                {
                    final double value = res.doubleValue();
                    // get index in color map
                    int index = (int) Math.round(((value - minValue) / range) * IcyColorMap.MAX_INDEX);
                    // need to reverse order ?
                    if (reverse)
                        index = IcyColorMap.MAX_INDEX - index;
                    // get color
                    color = colormap.getColor(index);
                }
                else
                    color = Color.black;

                // set color
                roi.setColor(color);
            }
        }
    }
}