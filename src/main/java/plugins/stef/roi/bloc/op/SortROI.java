/**
 * 
 */
package plugins.stef.roi.bloc.op;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.system.IcyExceptionHandler;
import icy.type.collection.CollectionUtil;
import icy.util.StringUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.gui.model.ValueSelectionModel;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarException;
import plugins.kernel.roi.descriptor.measure.ROIInteriorDescriptor;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to sort a set of ROIs using a specific descriptor.
 * 
 * @author Stephane
 */
public class SortROI extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    static class ValuedROI implements Comparable<ValuedROI>
    {
        ROI roi;
        Number value;
        int sortFactor;

        public ValuedROI(ROI roi, Object value, boolean reverse)
        {
            super();

            this.roi = roi;
            this.value = (value instanceof Number) ? (Number) value : null;
            sortFactor = reverse ? -1 : 1;
        }

        @Override
        public int compareTo(ValuedROI vr)
        {
            if (vr.value == value)
                return 0;

            if (value == null)
                return -1 * sortFactor;
            if (vr.value == null)
                return 1 * sortFactor;

            final double v1 = value.doubleValue();
            final double v2 = vr.value.doubleValue();

            if (v1 < v2)
                return -1 * sortFactor;
            else if (v1 > v2)
                return 1 * sortFactor;
            else
                return 0;
        }
    }

    final protected VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    final protected VarSequence sequence = new VarSequence("Sequence", null);
    final protected VarString descriptors = new VarString("Sort on", "");
    final protected VarBoolean reverse = new VarBoolean("Reverse", Boolean.FALSE);
    final protected VarROIArray output = new VarROIArray("Sorted ROI(s)");

    @Override
    public void run()
    {
        try
        {
            final List<ROI> result = SortROIs(CollectionUtil.asList(roiSet.getValue()), sequence.getValue(),
                    descriptors.getValue(), reverse.getValue().booleanValue());

            output.setValue(result.toArray(new ROI[result.size()]));
        }
        catch (IllegalArgumentException e)
        {
            throw new VarException(descriptors, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        final List<ROIDescriptor> roiDescriptors = new ArrayList<ROIDescriptor>(
                ROIDescriptor.getDescriptors().keySet());
        // build list of descriptor id
        final List<String> descriptorsId = new ArrayList<String>();
        String sizeDescriptorId = null;

        for (ROIDescriptor descriptor : roiDescriptors)
        {
            final String id = descriptor.getId();

            // keep trace of size descriptor
            if (StringUtil.equals(ROIInteriorDescriptor.ID, id))
                sizeDescriptorId = id;

            descriptorsId.add(id);
        }

        // alpha sort
        Collections.sort(descriptorsId);

        // initialize descriptors field
        descriptors.setDefaultEditorModel(new ValueSelectionModel<String>(
                descriptorsId.toArray(new String[descriptorsId.size()]), sizeDescriptorId, false));

        inputMap.add("roi", roiSet);
        inputMap.add("sequence", sequence);
        inputMap.add("descriptors", descriptors);
        inputMap.add("reverse", reverse);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }

    /**
     * Sort a set of ROIs on a specific descriptor.
     * 
     * @param rois
     *        input ROIS to filter
     * @param sequence
     *        input sequence used for ROI descriptor computation (can be null if not required)
     * @param descriptorId
     *        the {@link ROIDescriptor} id to use for filtering
     * @param reverse
     *        reverse sort order
     * @return sorted list of ROIS
     * @throws IllegalArgumentException
     *         if given ROI descriptor id is not found
     */
    public static List<ROI> SortROIs(Collection<ROI> rois, Sequence sequence, String descriptorId, boolean reverse)
            throws IllegalArgumentException
    {
        final List<ValuedROI> result = new ArrayList<ValuedROI>();
        final ROIDescriptor roiDescriptor = ROIDescriptor.getDescriptor(ROIDescriptor.getDescriptors().keySet(),
                descriptorId);

        if (roiDescriptor == null)
            throw new IllegalArgumentException("Cannot found '" + descriptorId + "' ROI descriptor !");

        for (ROI roi : rois)
        {
            if (roi != null)
            {
                try
                {
                    result.add(new ValuedROI(roi, roiDescriptor.compute(roi, sequence), reverse));
                }
                catch (Exception e)
                {
                    IcyExceptionHandler.showErrorMessage(e, false, true);
                    // if we can't compute the descriptor we use null value
                    result.add(new ValuedROI(roi, null, reverse));
                }
            }
        }

        // sort on descriptor
        Collections.sort(result);

        // build ROI list
        final List<ROI> resultROI = new ArrayList<ROI>();
        for (ValuedROI vr : result)
            resultROI.add(vr.roi);

        return resultROI;
    }
}