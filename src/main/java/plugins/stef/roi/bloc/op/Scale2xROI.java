/**
 * 
 */
package plugins.stef.roi.bloc.op;

import java.util.ArrayList;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to scale (power of 2 factor only) a set of ROIs (2D or 3D) along X, Y and Z axis.
 * 
 * @author Stephane
 */
public class Scale2xROI extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected final VarROIArray input = new VarROIArray("ROI(s)", null);
    protected final VarROIArray output = new VarROIArray("ROI(s)", null);
    protected final VarInteger factor = new VarInteger("Power 2 factor (2^x)", 0, null);
    protected final VarBoolean scaleZ = new VarBoolean("Scale Z", Boolean.TRUE);

    @Override
    public void run()
    {
        final ROI[] rois = input.getValue();
        final List<ROI> result = new ArrayList<ROI>();

        final int f = factor.getValue().intValue();
        final boolean sz = scaleZ.getValue().booleanValue();

        try
        {
            if (rois != null)
            {
                for (ROI roi : rois)
                {
                    if (roi != null)
                    {
                        ROI r = roi;
                        // number of iteration needed
                        int it = Math.abs(f);

                        // duplicate it as we expect a new ROI here
                        if (it == 0)
                            r = r.getCopy();
                        else
                        {
                            // scale
                            while (it-- > 0)
                                r = ROIUtil.get2XScaled(r, sz, f < 0);
                        }

                        result.add(r);
                    }
                }
            }

            output.setValue(result.toArray(new ROI[result.size()]));
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", input);
        inputMap.add("factor", factor);
        inputMap.add("scaleZ", scaleZ);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}