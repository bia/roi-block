/**
 * 
 */
package plugins.stef.roi.bloc.op;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.system.IcyExceptionHandler;
import icy.type.collection.CollectionUtil;
import icy.util.StringUtil;
import icy.util.StringUtil.AlphanumComparator;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.gui.model.ValueSelectionModel;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarException;
import plugins.kernel.roi.descriptor.measure.ROIInteriorDescriptor;
import plugins.kernel.roi.descriptor.property.ROIColorDescriptor;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to filter a set of ROIs using a specific descriptor.
 * 
 * @author Stephane
 */
public class FilterROI extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    public static enum CompareOperator
    {
        EQUAL
        {
            @Override
            public String toString()
            {
                return "Equal to";
            }
        },
        NOT_EQUAL
        {
            @Override
            public String toString()
            {
                return "Not equal to";
            }
        },
        LOWER
        {
            @Override
            public String toString()
            {
                return "Lower than";
            }
        },
        LOWER_OR_EQUAL
        {
            @Override
            public String toString()
            {
                return "Lower or equal than";
            }
        },
        GREATER
        {
            @Override
            public String toString()
            {
                return "Greater than";
            }
        },
        GREATER_OR_EQUAL
        {
            @Override
            public String toString()
            {
                return "Greater or equal than";
            }
        }
    }

    private static final AlphanumComparator comp = new AlphanumComparator();

    protected final VarROIArray roiSet = new VarROIArray("ROI(s)", null);
    protected final VarSequence sequence = new VarSequence("Sequence", null);
    protected final VarString descriptors = new VarString("Filter on", "");
    protected final VarEnum<CompareOperator> operator = new VarEnum<CompareOperator>("Accept if",
            CompareOperator.EQUAL);
    protected final VarString value = new VarString("Value", "0.0");
    // protected final VarDouble value = new VarDouble("Value", 0d);
    protected final VarROIArray output = new VarROIArray("Filtered ROI(s)");

    @Override
    public void run()
    {
        try
        {
            final List<ROI> result = filterROIs(CollectionUtil.asList(roiSet.getValue()), sequence.getValue(),
                    descriptors.getValue(), operator.getValue(), value.getValue());

            output.setValue(result.toArray(new ROI[result.size()]));
        }
        catch (IllegalArgumentException e)
        {
            throw new VarException(descriptors, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        final List<ROIDescriptor> roiDescriptors = new ArrayList<ROIDescriptor>(
                ROIDescriptor.getDescriptors().keySet());
        // build list of descriptor id
        final List<String> descriptorsId = new ArrayList<String>();
        String sizeDescriptorId = null;

        for (ROIDescriptor descriptor : roiDescriptors)
        {
            final String id = descriptor.getId();

            // keep trace of size descriptor
            if (StringUtil.equals(ROIInteriorDescriptor.ID, id))
                sizeDescriptorId = id;

            descriptorsId.add(id);
        }

        // alpha sort
        Collections.sort(descriptorsId);

        // initialize descriptors field
        descriptors.setDefaultEditorModel(new ValueSelectionModel<String>(
                descriptorsId.toArray(new String[descriptorsId.size()]), sizeDescriptorId, false));

        inputMap.add("roi", roiSet);
        inputMap.add("sequence", sequence);
        inputMap.add("descriptors", descriptors);
        inputMap.add("operator", operator);
        inputMap.add("value", value);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }

    private static Object getTypedValue(String value)
    {
        try
        {
            return Double.valueOf(Double.parseDouble(value.replace(',', '.')));
        }
        catch (NumberFormatException e1)
        {
            try
            {
                return Double.valueOf(Float.parseFloat(value.replace('.', ',')));
            }
            catch (NumberFormatException e2)
            {
                try
                {
                    return Long.valueOf(Long.parseLong(value));
                }
                catch (NumberFormatException e4)
                {
                    // probably not a number then...
                }
            }
        }

        // ok so this is probably just a String
        return value;
    }

    private static String colorToString(Color color)
    {
        return StringUtil.toHexaString(color.getAlpha(), 2) + StringUtil.toHexaString(color.getRed(), 2)
                + StringUtil.toHexaString(color.getGreen(), 2) + StringUtil.toHexaString(color.getBlue(), 2);
    }

    private static boolean compareNumber(CompareOperator op, double value1, double value2)
    {
        // compute comparison
        final int compResult = Double.compare(value1, value2);

        switch (op)
        {
            default:
            case EQUAL:
                return compResult == 0;
            case NOT_EQUAL:
                return compResult != 0;
            case LOWER:
                return compResult < 0;
            case LOWER_OR_EQUAL:
                return compResult <= 0;
            case GREATER:
                return compResult > 0;
            case GREATER_OR_EQUAL:
                return compResult >= 0;
        }
    }

    private static boolean compareString(CompareOperator op, String value1, String value2, String value2Regex)
    {
        switch (op)
        {
            default:
            case EQUAL:
                return value1.matches(value2Regex);
            case NOT_EQUAL:
                return !value1.matches(value2Regex);
            case LOWER:
                return comp.compare(value1, value2) < 0;
            case LOWER_OR_EQUAL:
                return comp.compare(value1, value2) <= 0;
            case GREATER:
                return comp.compare(value1, value2) > 0;
            case GREATER_OR_EQUAL:
                return comp.compare(value1, value2) >= 0;
        }
    }

    /**
     * Filter a set of ROIs using a specific descriptor and the compare operation (keep ROIS where result of comparison is true).
     * 
     * @param rois
     *        input ROIS to filter
     * @param sequence
     *        input sequence used for ROI descriptor computation (can be null if not required)
     * @param descriptorId
     *        the {@link ROIDescriptor} id to use for filtering
     * @param op
     *        Compare operation to use
     * @param value
     *        the value for the comparison (can be a Number or just a String)
     * @return filtered list of ROIS
     * @throws IllegalArgumentException
     *         if given ROI descriptor id is not found
     */
    public static List<ROI> filterROIs(Collection<ROI> rois, Sequence sequence, String descriptorId, CompareOperator op,
            String value) throws IllegalArgumentException
    {
        final List<ROI> result = new ArrayList<ROI>();
        final ROIDescriptor roiDescriptor = ROIDescriptor.getDescriptor(ROIDescriptor.getDescriptors().keySet(),
                descriptorId);

        if (roiDescriptor == null)
            throw new IllegalArgumentException("Cannot found '" + descriptorId + "' ROI descriptor !");

        // try to get the type behind
        final Object typedValue = getTypedValue(value);
        // get regex from String
        final String regexValue;
        if (roiDescriptor instanceof ROIColorDescriptor)
        {
            regexValue = StringUtil.wildcardToRegex(value.toLowerCase());
            value = value.toLowerCase();
        }
        else
        {
            regexValue = StringUtil.wildcardToRegex(value);
        }

        // number comparison
        if (typedValue instanceof Number)
        {
            final double doubleValue = ((Number) typedValue).doubleValue();

            for (ROI roi : rois)
            {
                if (roi != null)
                {
                    try
                    {
                        final Object res = roiDescriptor.compute(roi, sequence);
                        final boolean accepted;

                        // get double value if possible
                        if (res instanceof Number)
                            accepted = compareNumber(op, ((Number) res).doubleValue(), doubleValue);
                        else if (res instanceof Color)
                            accepted = compareNumber(op, ((Color) res).getRGB(), doubleValue);
                        // can't filter --> show error message
                        else if (res instanceof String)
                            accepted = compareString(op, (String) res, value, regexValue);
                        else if (res != null)
                            accepted = compareString(op, res.toString(), value, regexValue);
                        else
                            // don't accept when result is null
                            accepted = false;
                        // throw new VarException(null,
                        // "Descriptor result is null, can't apply filtering criterion !");

                        if (accepted)
                            result.add(roi);
                    }
                    catch (VarException e1)
                    {
                        throw e1;
                    }
                    catch (Exception e2)
                    {
                        IcyExceptionHandler.showErrorMessage(e2, false, true);
                        // if we can't compute the descriptor we accept the ROI by default...
                        result.add(roi);
                    }
                }
            }
        }
        else if (typedValue instanceof String)
        {
            for (ROI roi : rois)
            {
                if (roi != null)
                {
                    try
                    {
                        final Object res = roiDescriptor.compute(roi, sequence);
                        final String stringRes;

                        // not really safe to compare number with a String here
                        if (res instanceof Number)
                            throw new VarException(null,
                                    "Can't apply filtering criterion, you need to provide a valid number in 'Value' field.");
                        // stringRes = ((Number) res).toString();
                        else if (res instanceof String)
                            stringRes = (String) res;
                        else if (res instanceof Color)
                            stringRes = colorToString((Color) res).toLowerCase();
                        else if (res != null)
                            stringRes = res.toString();
                        else
                            stringRes = "";

                        if (compareString(op, stringRes, value, regexValue))
                            result.add(roi);
                    }
                    catch (VarException e1)
                    {
                        throw e1;
                    }
                    catch (Exception e2)
                    {
                        IcyExceptionHandler.showErrorMessage(e2, false, true);
                        // if we can't compute the descriptor we accept the ROI by default...
                        result.add(roi);
                    }
                }
            }
        }

        return result;

    }

    /**
     * Filter a set of ROIs using a specific descriptor and the compare operation (keep ROIS where result of comparison is true).
     * 
     * @param rois
     *        input ROIS to filter
     * @param sequence
     *        input sequence used for ROI descriptor computation (can be null if not required)
     * @param descriptorId
     *        the {@link ROIDescriptor} id to use for filtering
     * @param op
     *        Compare operation to use
     * @param value
     *        the value for the comparison
     * @return filtered list of ROIS
     * @throws IllegalArgumentException
     *         if given ROI descriptor id is not found
     */
    public static List<ROI> filterROIs(Collection<ROI> rois, Sequence sequence, String descriptorId, CompareOperator op,
            double value) throws IllegalArgumentException
    {
        return filterROIs(rois, sequence, descriptorId, op, Double.toString(value));
    }
}
