package plugins.stef.roi.bloc.op;

import java.util.ArrayList;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to separate unconnected regions from input ROI(s).<br>
 * Only 2D ROI(s) are supported right now.
 * 
 * @author Stephane
 */
public class SeparateROIComponents extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected final VarROIArray input;
    protected final VarROIArray output;

    public SeparateROIComponents()
    {
        super();

        input = new VarROIArray("ROI(s)");
        output = new VarROIArray("Separated components ROI(s)");
    }

    @Override
    public void run()
    {
        final ROI[] rois = input.getValue();
        final List<ROI> result = new ArrayList<ROI>();

        try
        {
            if (rois != null)
            {
                for (ROI roi : rois)
                    for (ROI comp : ROIUtil.getConnectedComponents(roi))
                        if (comp != null)
                            result.add(comp);
            }

            output.setValue(result.toArray(new ROI[result.size()]));
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", input);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
