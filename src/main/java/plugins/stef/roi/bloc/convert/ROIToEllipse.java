/**
 * 
 */
package plugins.stef.roi.bloc.convert;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to convert input ROI(s) to 2D ellipse type ROI(s) centered on the mass center.<br>
 * 
 * @author Stephane
 */
public class ROIToEllipse extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected final VarROIArray input;
    protected final VarDouble radiusX;
    protected final VarDouble radiusY;
    protected final VarROIArray output;

    public ROIToEllipse()
    {
        super();

        input = new VarROIArray("Roi(s)");
        radiusX = new VarDouble("Radius X", 1d);
        radiusY = new VarDouble("Radius Y", 1d);
        output = new VarROIArray("Ellipse roi(s)");
    }

    @Override
    public void run()
    {
        final ROI[] rois = input.getValue();
        final ROI[] result = new ROI[(rois != null) ? rois.length : 0];
        final double rx = radiusX.getValue().doubleValue();
        final double ry = radiusY.getValue().doubleValue();

        try
        {
            if (rois != null)
            {
                for (int r = 0; r < rois.length; r++)
                    result[r] = ROIUtil.convertToEllipse(rois[r], rx, ry);
            }
        }
        catch (InterruptedException e)
        {
            System.err.println("ROIToEllipse process interrupted !");
            output.setValue(null);
            return;
        }

        output.setValue(result);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", input);
        inputMap.add("radiusX", radiusX);
        inputMap.add("radiuxY", radiusY);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
