package plugins.stef.roi.bloc.convert;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROIUtil;
import icy.type.DataType;
import icy.type.collection.CollectionUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block (Protocols) to convert ROI(s) (Region Of Interest) into a binary image / sequence.<br>
 * <br>
 * It takes a ROI (or a list of ROI) as input and generate a binary sequence from it.<br>
 * The content of the ROI area is filled with the specified value while background remains at 0.<br>
 * Size informations are used to define the dimension of the output sequence, a value of 0 indicate
 * to use the ROI dimension instead.
 * 
 * @author Stephane
 */
public class RoiToSequence extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    final VarROIArray inputROI;
    final VarInteger sizeX;
    final VarInteger sizeY;
    final VarInteger sizeC;
    final VarInteger sizeZ;
    final VarInteger sizeT;
    final VarEnum<DataType> dataType;
    final VarDouble fillValue;
    final VarBoolean labeled;
    final VarSequence varOut;

    public RoiToSequence()
    {
        super();

        inputROI = new VarROIArray("ROI(s)");
        sizeX = new VarInteger("Size X", 0);
        sizeY = new VarInteger("Size Y", 0);
        sizeC = new VarInteger("Size C", 1);
        sizeZ = new VarInteger("Size Z", 1);
        sizeT = new VarInteger("Size T", 1);
        dataType = new VarEnum<DataType>("Data type", DataType.UBYTE);
        fillValue = new VarDouble("Fill value", 255.0);
        labeled = new VarBoolean("Labeled", Boolean.FALSE);
        varOut = new VarSequence("Sequence", null);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", inputROI);
        inputMap.add("sizeX", sizeX);
        inputMap.add("sizeY", sizeY);
        inputMap.add("sizeC", sizeC);
        inputMap.add("sizeZ", sizeZ);
        inputMap.add("sizeT", sizeT);
        inputMap.add("dataType", dataType);
        // inputMap.add("fillValue", fillValue);
        inputMap.add("labeled", labeled);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Sequence", varOut);
    }

    @Override
    public void run()
    {
        try
        {
            varOut.setValue(
                    ROIUtil.convertToSequence(CollectionUtil.asList(inputROI.getValue()), sizeX.getValue().intValue(),
                            sizeY.getValue().intValue(), sizeC.getValue().intValue(), sizeZ.getValue().intValue(),
                            sizeT.getValue().intValue(), dataType.getValue(), labeled.getValue().booleanValue()));
        }
        catch (InterruptedException e)
        {
            // ignore
            throw new VarException(varOut, e.getMessage());
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
