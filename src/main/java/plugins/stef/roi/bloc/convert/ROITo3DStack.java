package plugins.stef.roi.bloc.convert;

import java.util.ArrayList;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROIUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.util.VarException;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to convert input 2D ROIs to 3D stack ROI by stacking them along the Z axis.
 * 
 * @author Stephane
 */
public class ROITo3DStack extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected final VarROIArray input;
    protected final VarInteger sizeZ;
    protected final VarROIArray output;

    public ROITo3DStack()
    {
        super();

        input = new VarROIArray("Roi(s)");
        sizeZ = new VarInteger("Size Z", 10);
        output = new VarROIArray("3D stack roi(s)");
    }

    @Override
    public void run()
    {
        final ROI[] rois = input.getValue();
        final List<ROI> result = new ArrayList<ROI>();

        try
        {
            if (rois != null)
            {
                final int maxZ = sizeZ.getValue().intValue() - 1;
                if (maxZ < 0)
                    throw new VarException(sizeZ, "Size Z field should be > 0");

                for (ROI roi : rois)
                {
                    // only convert 2D roi
                    if (roi instanceof ROI2D)
                        result.add(ROIUtil.convertToStack((ROI2D) roi, 0, maxZ));
                    else
                        // otherwise we add them directly to the result
                        result.add(roi);
                }
            }

            output.setValue(result.toArray(new ROI[result.size()]));
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", input);
        inputMap.add("sizeZ", sizeZ);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}