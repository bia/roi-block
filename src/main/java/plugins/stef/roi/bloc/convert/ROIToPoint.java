/**
 * 
 */
package plugins.stef.roi.bloc.convert;

import java.util.ArrayList;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to convert input ROI(s) to single point type ROI(s) representing the mass center.
 * 
 * @author Stephane
 */
public class ROIToPoint extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected final VarROIArray input;
    protected final VarROIArray output;

    public ROIToPoint()
    {
        super();

        input = new VarROIArray("Roi(s)");
        output = new VarROIArray("Point roi(s)");
    }

    @Override
    public void run()
    {
        final ROI[] rois = input.getValue();
        final List<ROI> result = new ArrayList<ROI>();

        try
        {
            if (rois != null)
            {
                for (ROI roi : rois)
                    result.add(ROIUtil.convertToPoint(roi));
            }
        }
        catch (InterruptedException e)
        {
            System.err.println("ROIToPoint process interrupted !");
            output.setValue(null);
            return;
        }

        output.setValue(result.toArray(new ROI[result.size()]));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", input);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
