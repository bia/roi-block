/**
 * 
 */
package plugins.stef.roi.bloc.convert;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to convert input ROI(s) to 2D rectangle type ROI(s) centered on the mass center.<br>
 * 
 * @author Stephane
 */
public class ROIToRectangle extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected final VarROIArray input;
    protected final VarDouble width;
    protected final VarDouble height;
    protected final VarROIArray output;

    public ROIToRectangle()
    {
        super();

        input = new VarROIArray("Roi(s)");
        width = new VarDouble("Width", 1d);
        height = new VarDouble("Height", 1d);
        output = new VarROIArray("Rectangle roi(s)");
    }

    @Override
    public void run()
    {
        final ROI[] rois = input.getValue();
        final ROI[] result = new ROI[(rois != null) ? rois.length : 0];
        final double w = width.getValue().doubleValue();
        final double h = height.getValue().doubleValue();

        try
        {
            if (rois != null)
            {
                for (int r = 0; r < rois.length; r++)
                    result[r] = ROIUtil.convertToRectangle(rois[r], w, h);
            }
        }
        catch (InterruptedException e)
        {
            System.err.println("ROIToRectangle process interrupted !");
            output.setValue(null);
            return;
        }

        output.setValue(result);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", input);
        inputMap.add("width", width);
        inputMap.add("height", height);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output", output);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}
