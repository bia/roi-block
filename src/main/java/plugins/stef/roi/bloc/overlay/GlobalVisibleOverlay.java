/**
 * 
 */
package plugins.stef.roi.bloc.overlay;

import icy.canvas.IcyCanvas;
import icy.canvas.Layer;
import icy.canvas.Layer.LayerListener;
import icy.painter.Overlay;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.util.StringUtil;
import icy.util.XMLUtil;

import java.awt.Graphics2D;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import plugins.kernel.roi.roi2d.ROI2DPoint;

/**
 * This special Overlay allow to provide a global visible property for a given list of Overlay.
 * 
 * @author Stephane
 */
public class GlobalVisibleOverlay extends Overlay implements LayerListener
{
    public static final String ID = "GlobalVisibleOverlay";

    public static final String ID_OVERLAY = "overlay";

    public static final String ID_CLASSNAME = "classname";
    public static final String ID_ID = "id";
    public static final String ID_NAME = "name";
    public static final String ID_PRIORITY = "priority";
    public static final String ID_READONLY = "readOnly";
    public static final String ID_CANBEREMOVED = "canBeRemoved";
    public static final String ID_RECEIVEKEYEVENTONHIDDEN = "receiveKeyEventOnHidden";
    public static final String ID_RECEIVEMOUSEEVENTONHIDDEN = "receiveMouseEventOnHidden";

    public static final String ID_ROIS = "rois";
    public static final String ID_ROI = "roi";
    public static final String ID_ROI_ID = "roiId";

    protected final Set<Overlay> overlays;
    protected final Map<Layer, IcyCanvas> layerMap;

    public GlobalVisibleOverlay(String name, List<Overlay> overlays)
    {
        super(name);

        this.overlays = (overlays != null) ? new HashSet<Overlay>(overlays) : new HashSet<Overlay>();
        layerMap = new HashMap<Layer, IcyCanvas>();
    }

    public GlobalVisibleOverlay(List<Overlay> overlays)
    {
        this("Global visible", overlays);
    }

    public GlobalVisibleOverlay()
    {
        this("Global visible", null);
    }

    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {
        final Layer layer = canvas.getLayer(this);

        if (!layerMap.containsKey(layer))
        {
            layerMap.put(layer, canvas);
            // update visible state if needed
            layerChanged(layer, Layer.PROPERTY_VISIBLE);
            // then listen layer events
            layer.addListener(this);

            // save metadata
            saveToXML(sequence, sequence.getNode(ID));
        }
    }

    @Override
    public void layerChanged(Layer source, String propertyName)
    {
        // only interested by visible property change
        if (StringUtil.equals(propertyName, Layer.PROPERTY_VISIBLE))
        {
            final IcyCanvas canvas = layerMap.get(source);

            if (canvas != null)
            {
                final boolean visible = source.isVisible();

                for (Overlay overlay : overlays)
                {
                    final Layer layer = canvas.getLayer(overlay);

                    // set visible property
                    if (layer != null)
                        layer.setVisible(visible);
                }
            }
        }
    }

    public boolean loadFromXML(Sequence sequence, Node node)
    {
        if (node == null)
            return false;

        beginUpdate();
        try
        {
            setName(XMLUtil.getElementValue(node, ID_NAME, ""));
            setPriority(OverlayPriority.values()[XMLUtil.getElementIntValue(node, ID_PRIORITY,
                    OverlayPriority.SHAPE_NORMAL.ordinal())]);
            setReadOnly(XMLUtil.getElementBooleanValue(node, ID_READONLY, false));
            setReceiveKeyEventOnHidden(XMLUtil.getElementBooleanValue(node, ID_RECEIVEKEYEVENTONHIDDEN, false));
            setReceiveMouseEventOnHidden(XMLUtil.getElementBooleanValue(node, ID_RECEIVEMOUSEEVENTONHIDDEN, false));

            overlays.clear();
            // get list of ROI sorted on id
            final List<ROI> rois = sequence.getROIs(true);
            SearchROI searchRoi = new SearchROI();

            final List<Node> nodesId = XMLUtil.getChildren(XMLUtil.getElement(node, ID_ROIS), ID_ROI);
            if (nodesId != null)
            {
                for (Node n : nodesId)
                {
                    searchRoi.setId(XMLUtil.getElementIntValue(n, ID_ROI_ID, -1));

                    // id correctly read from XML
                    if (searchRoi.getId() != -1)
                    {
                        final int ind = Collections.binarySearch(rois, searchRoi, ROI.idComparator);

                        // found ? --> get the ROI overlay
                        if (ind >= 0)
                            overlays.add(rois.get(ind).getOverlay());
                    }
                }
            }
        }
        finally
        {
            endUpdate();
        }

        return true;
    }

    public boolean saveToXML(Sequence sequence, Node node)
    {
        if (node == null)
            return false;

        // clear node
        XMLUtil.removeAllChildren(node);

        // then save
        XMLUtil.setElementValue(node, ID_CLASSNAME, getClass().getName());
        XMLUtil.setElementIntValue(node, ID_ID, id);
        XMLUtil.setElementValue(node, ID_NAME, getName());
        XMLUtil.setElementIntValue(node, ID_PRIORITY, getPriority().ordinal());
        XMLUtil.setElementBooleanValue(node, ID_READONLY, isReadOnly());
        XMLUtil.setElementBooleanValue(node, ID_RECEIVEKEYEVENTONHIDDEN, getReceiveKeyEventOnHidden());
        XMLUtil.setElementBooleanValue(node, ID_RECEIVEMOUSEEVENTONHIDDEN, getReceiveMouseEventOnHidden());

        // get ROI from Sequence
        final List<ROI> rois = sequence.getROIs();
        for (int i = rois.size() - 1; i >= 0; i--)
        {
            final ROI roi = rois.get(i);
            // the overlay of this ROI is not here ? --> remove ROIs from list
            if (!overlays.contains(roi.getOverlay()))
                rois.remove(i);
        }

        final Element nodeRois = XMLUtil.setElement(node, ID_ROIS);
        // save ROIS id
        for (ROI roi : rois)
        {
            final Element nodeRoi = XMLUtil.addElement(nodeRois, ID_ROI);
            XMLUtil.setElementIntValue(nodeRoi, ID_ROI_ID, roi.getId());
        }

        return true;
    }

    // dummy ROI for search operation
    private class SearchROI extends ROI2DPoint
    {
        public SearchROI()
        {
            super();
        }

        public void setId(int value)
        {
            id = value;
        }
    }
}
