/**
 * 
 */
package plugins.stef.roi.bloc.overlay;

import icy.painter.Overlay;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;

import java.util.ArrayList;
import java.util.List;

import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarString;
import plugins.stef.roi.bloc.RoiBlocks;

/**
 * Block to create a special Overlay which allow to display/hide a group of ROI (Region Of Interest).
 * 
 * @author Stephane
 */
public class CreateGlobalVisibleOverlay extends Plugin implements ROIBlock, PluginLibrary, PluginBundled
{
    protected VarROIArray inputRois = new VarROIArray("ROI(s)");
    protected VarString overlayName = new VarString("Overlay name", "Global visible");
    protected Var<Overlay> overlay = new Var<Overlay>("Overlay", Overlay.class);

    @Override
    public void run()
    {
        final ROI[] rois = inputRois.getValue();
        final List<Overlay> roiOverlays = new ArrayList<Overlay>();

        for (ROI roi : rois)
            roiOverlays.add(roi.getOverlay());

        overlay.setValue(new GlobalVisibleOverlay(overlayName.getValue(), roiOverlays));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("rois", inputRois);
        inputMap.add("overlayName", overlayName);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("overlay", overlay);
    }

    @Override
    public String getMainPluginClassName()
    {
        return RoiBlocks.class.getName();
    }
}